<!DOCTYPE html>
<html>
<head>
	<title>Inicio</title>
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="js/librerias/bootstrap-3.3.7-dist/css/bootstrap.css">
	<link rel="icon" href="img/dinoico.ico" type="image/x-icon" />
	<script type="text/javascript" src="js/librerias/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/librerias/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="principal">
		<div class="banda_negras"></div>
		<div class="contenidoP">
			<div class="lb">	
				<div class="cabecera"></div>
			</div>
			<div class="menu">
				<div class="caja_boton dropdown">
					<button class= "boton" id="menu1" type="button" data-toggle="dropdown">Acerca de nosotros
					</button>
					<ul class="dropdown-menu " role="menu" aria-labelledby="menu1">
						<li role="presentation"><a role="menuitem" tabindex="-1" href="index.php">Quienes somos</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="pag/nosotros-des/eljuego.php">El juego</a></li>  
						<li role="presentation"><a role="menuitem" tabindex="-1" href="pag/nosotros-des/repositorio.php">Repositorio</a></li>    
					</ul>
				</div>
				<div class="caja_boton  dropdown">
					<button class= "boton" id="menu1" type="button" data-toggle="dropdown">Jugar
					</button>
					<ul class="dropdown-menu caja_menu" role="menu2" aria-labelledby="menu2">
						<li role="presentation"><a role="menuitem" tabindex="-2" href="pag/jugar-des/juego.php">Juego</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-2" href="pag/jugar-des/ranking.php">Ranking</a></li>
					</ul>
				</div>

				<div class="caja_boton dropdown">
					<?php
					if(isset($_COOKIE['inicio'])){
						if ($_COOKIE['inicio']==1){
							?>
							<button class= "boton" id="boton1" type="button" data-toggle="dropdown">Bienvenido <?php echo $_COOKIE['name']; ?> </button>
							<ul class="dropdown-menu caja_menu" role="menu3" aria-labelledby="menu3">
								<li role="presentation"><a role="menuitem" tabindex="-2" href="pag/cerrar.php">Cerrar Sesion</a></li>
							</ul>
							<?php
						}else{
							?>	
							<button class= "boton" id="boton2" type="button"  onclick="window.location.href='pag/login-registro.php'">Log-in / Registro</button>
							<?php
						}
					}else{
						?>	
						<button class= "boton" id="boton2" type="button"  onclick="window.location.href='pag/login-registro.php'">Log-in / Registro</button>
						<?php	
					}
					?>
				</div>

			</div>

			<div class="contenido" >
				<div class="contenidoI">
					<h2>Bienvenido a Beat Down</h2>
					<p >Un juego creado por tres estudiantes con ambiciones y que quieren experimentar en el mundo de los videojuegos. Puedes visitar cualquier parte de la página y conocernos un poco más , o saber 	quien va más alto en nuestro ranking. <br>
						La unica condicion que te pedimos para jugar es que estés registrado o logueado, puedes pinchar en las palabras anteriores o en el menú. <br> <br>

						Beat Down es un juego que hereda el espíritu clásico de los arcade top-down por rondas, un shooter algo difícil al que te tendrás que acomodar antes de empezar a pasar rondas. Cada tres rondas tendrás un enemigo que te soltará un arma y munición para la misma. <br> <br>

						Puedes moverte con WASD, como siempre, disparar y apuntar con el ratón, pausar con la P y cambiar de arma con el espacio. <br> <br>

						Que disfrutes del juego. <br> <br>

						Puedes seguir todos nuestro avances a través de nuestras redes sociales que te dejamos abajo y también ponerte en contacto con nosotros a través de contacto@beatdown.xyz . </p>

						<h2>El equipo</h2>
						<p>	
							Álvaro González (MoebiusMachine): Le apasionan los nuevos frameworks y es un fan total de Python.Prefiere bajar al código a manejarse con las interfaces gráficas y siempre tiene la cabeza metida en algún lío de programación. En este proyecto ha colaborado desarrollando el videojuego. <br>
							Email: avr.gzz@gmail.com   <br> <br>

							Iván Sánchez Arévalo: apasionado de la informática y enamorado de los videojuegos. Siempre le gusta adentrarse en retos que le lleven a exponer sus conocimientos en la programación. Le encanta trastear y aprender cómo picar código de una forma más eficiente y atractiva. <br>
							Email: isan_valo@msn.com  <br> <br>

							Guillermo Garcia Erezuma: Aprendiz de muchas disciplinas especialmente las de videojuegos, deportes, programación, audiovisuales, etc. Le encanta estar al dia de todo tipo de  tecnología nueva que van sacando y testear programas nuevos. <br>
							Email: guillermo_g_e12@hotmail.com  <br> <br>
						</p>
						<div class="twitter">
							<a href="https://twitter.com/share" class="twitter-share-button centro" data-url="beatdown.xyz" data-text="Prueba este nuevo juego indie creado por 3 desarrolladores" data-via="beatdownstudios" data-size="large">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

							<a href="https://twitter.com/beatdownstudios" class="twitter-follow-button centro" data-show-count="false" data-size="large">Follow @beatdown</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
			<p>Beat Down @ Copyright</p>
		</div>
	</div>
	<div class="banda_negras2"></div>
</div>
</body>
</html>