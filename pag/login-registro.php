<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="icon" href="../img/dinoico.ico" type="image/x-icon" />
	<script type="text/javascript" src="./cambiarPestanna.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/librerias/bootstrap-3.3.7-dist/css/bootstrap.css">
	<script type="text/javascript" src="../js/librerias/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/librerias/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../pag/cambiarPestanna.js"></script>
	<title>Login/Registro</title>
	
</head>
<body>
	<div class="principal">
		<div class="banda_negras"></div>
		<div class="contenidoP">
			<div class="lb" onclick="inicio()">	
				<div class="cabecera" ></div>
			</div>
			<div class="menu">
				<div class="caja_boton dropdown">
					<button class= "boton" id="menu1" type="button" data-toggle="dropdown">Acerca de nosotros
					</button>
					<ul class="dropdown-menu " role="menu" aria-labelledby="menu1">
						<li role="presentation"><a role="menuitem" tabindex="-1" href="../index.php">Quienes somos</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="nosotros-des/eljuego.php">El juego</a></li>  
						<li role="presentation"><a role="menuitem" tabindex="-1" href="nosotros-des/repositorio.php">Repositorio</a></li>    
					</ul>
				</div>
				<div class="caja_boton  dropdown">
					<button class= "boton" id="menu1" type="button" data-toggle="dropdown">Jugar
					</button>
					<ul class="dropdown-menu caja_menu" role="menu2" aria-labelledby="menu2">
						<li role="presentation"><a role="menuitem" tabindex="-2" href="jugar-des/juego.php">Juego</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-2" href="jugar-des/ranking.php">Ranking</a></li>
					</ul>
				</div>
				<div class="caja_boton">
					<?php
					if(isset($_COOKIE['inicio'])){
						if ($_COOKIE['inicio']==1){
							?>
							<button class= "boton" id="boton1" type="button" data-toggle="dropdown">Bienvenido <?php echo $_COOKIE['name']; ?> </button>
							<ul class="dropdown-menu caja_menu" role="menu2" aria-labelledby="menu2">
								<li role="presentation"><a role="menuitem" tabindex="-2" href="cerrar.php">Cerrar Sesion</a></li>
							</ul>
							<?php
						}else{
							?>	
							<button class= "boton" id="boton2" type="button"  onclick="window.location.href='login-registro.php'">Log-in / Registro</button>
							<?php	
						}
					}else{
						?>	
						<button class= "boton" id="boton2" type="button"  onclick="window.location.href='login-registro.php'">Log-in / Registro</button>
						<?php	
					}
					?>	             
				</div>

			</div>

			<div class="loginprincipal">
				<div>
					<div>
						<ul class="lista" >
							<div id="login2" class="pestanaldiv" onclick='login()'>  <li class="pestanal" id="login"> Login </li> </div>
							<div id="registro2" class="pestanardiv" onclick='registro()'> <li class="pestanar gris" id="registro"> Registro </li> </div>
						</ul>
					</div>
					<div class="formu">
						<div class="cuadro1" id="cpestana1">
							<form method="post" action="login.php">
								<table class="tabla">
									<tr>
										<th class="trinputs">Usuario</th>
										<td><input type="text" name="user" required></td>
									</tr>
									<tr>
										<th class="trinputs">Contraseña</th>
										<td><input type="password" name="pass" required></td>
									</tr>
									<tr>
										<td><input type="submit" name="enviar" value="Iniciar Sesion"></td>
									</tr>
								</table>    
							</form>
						</div>
						<div class="cuadro2" id="cpestana2" style="display:none;">
							<form method="post" action="registro.php">
								<table class="tabla">
									<tr >
										<th class="trinputs">Usuario</th>
										<td><input type="text" name="user" required></td>
									</tr>
									<tr>
										<th class="trinputs">Email</th>
										<td><input type="email" name="email" required></td>
									</tr>
									<tr>
										<th class="trinputs">Contraseña</th>
										<td><input type="password" name="pass" required></td>
									</tr>
									<tr>
										<th class="trinputs">Confirmar Contraseña</th>
										<td><input type="password" name="pass2" required></td>
									</tr>
									<tr>
										<td><input type="submit" name="enviar2" value="Registrarse"></td>
									</tr>
								</table>    
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="banda_negras2"></div>
	</div>
</body>
</html>