<!DOCTYPE html>
<html>
<head>
	<title>500</title>
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../../js/librerias/bootstrap-3.3.7-dist/css/bootstrap.css">
	<link rel="icon" href="../../img/dinoico.ico" type="image/x-icon" />
	<script type="text/javascript" src="../../js/librerias/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../../js/librerias/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="principal">
		<div class="banda_negras"></div>
		<div class="contenidoP">
			<div class="lb">
				<!--<div class="logo">
					<img src="../../img/logo3.png">
				</div>-->
				<div class="cabecera">
				</div>
			</div>
			<div class="menu">
				<div class="caja_boton dropdown">
					<button class= "boton" id="menu1" type="button" data-toggle="dropdown">Acerca de nosotros
					</button>
					<ul class="dropdown-menu " role="menu" aria-labelledby="menu1">
						<li role="presentation"><a role="menuitem" tabindex="-1" href="../../index.php">Quienes somos</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="../nosotros-des/juego.php">El juego</a></li>  
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Repositorio</a></li>    
					</ul>
				</div>
				<div class="caja_boton  dropdown">
					<button class= "boton" id="menu1" type="button" data-toggle="dropdown">Jugar
					</button>
					<ul class="dropdown-menu caja_menu" role="menu2" aria-labelledby="menu2">
						<li role="presentation"><a role="menuitem" tabindex="-2" href="../jugar-des/juego.php">Juego</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-2" href="../jugar-des/ranking.php">Ranking</a></li>
					</ul>
				</div>
				<div class="caja_boton dropdown">

					<?php
					if(isset($_COOKIE['inicio'])){
						
						if ($_COOKIE['inicio']==1){
							?>
							<button class= "boton" id="boton1" type="button" data-toggle="dropdown">Bienvenido <?php echo $_COOKIE['name']; ?> </button>
							<ul class="dropdown-menu caja_menu" role="menu2" aria-labelledby="menu2">
								<li role="presentation"><a role="menuitem" tabindex="-2" href="../cerrar.php">Cerrar Sesion</a></li>
							</ul>
							<?php
						}else{
							?>	
							<button class= "boton" id="boton2" type="button"  onclick="window.location.href='../login-registro.php'">Log-in / Registro</button>
							<?php	
						}
					}else{
						?>	
						<button class= "boton" id="boton2" type="button"  onclick="window.location.href='../login-registro.php'">Log-in / Registro</button>
						<?php	
					}
					?>	             


				</div>

			</div>

			<div class="contenido" >
				<h1>Error 500: Error interno del Servidor</h1>
			</div>
			<div class="footer">
				<p>Beat Down @ Copyright</p>
			</div>
		</div>
		<div class="banda_negras2"></div>
	</div>
</body>
</html>