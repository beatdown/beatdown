<!DOCTYPE html>
<html>
<head>
	<title>El juego</title>
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<meta charset="utf-8">
	<link rel="icon" href="../../img/dinoico.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="../../js/librerias/bootstrap-3.3.7-dist/css/bootstrap.css">
	<script type="text/javascript" src="../../js/librerias/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../../js/librerias/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../cambiarPestanna.js"></script>	
</head>
<body>
	<div class="principal">
		<div class="banda_negras"></div>
		<div class="contenidoP">
			<div class="lb" onclick="inicio()">
				<div class="cabecera">
				</div>
			</div>
			<div class="menu">
				<div class="caja_boton dropdown">
					<button class= "boton" id="menu1" type="button" data-toggle="dropdown">Acerca de nosotros
					</button>
					<ul class="dropdown-menu " role="menu" aria-labelledby="menu1">
						<li role="presentation"><a role="menuitem" tabindex="-1" href="../../index.php">Quienes somos</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="juego.php">El juego</a></li>  
						<li role="presentation"><a role="menuitem" tabindex="-1" href="repositorio.php">Repositorio</a></li>    
					</ul>
				</div>
				<div class="caja_boton  dropdown">
					<button class= "boton" id="menu1" type="button" data-toggle="dropdown">Jugar
					</button>
					<ul class="dropdown-menu caja_menu" role="menu2" aria-labelledby="menu2">
						<li role="presentation"><a role="menuitem" tabindex="-2" href="../jugar-des/juego.php">Juego</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-2" href="../jugar-des/ranking.php">Ranking</a></li>
					</ul>
				</div>
				<div class="caja_boton dropdown">

					<?php
					if(isset($_COOKIE['inicio'])){
						
						if ($_COOKIE['inicio']==1){
							?>
							<button class= "boton" id="boton1" type="button" data-toggle="dropdown">Bienvenido <?php echo $_COOKIE['name']; ?> </button>
							<ul class="dropdown-menu caja_menu" role="menu2" aria-labelledby="menu2">
								<li role="presentation"><a role="menuitem" tabindex="-2" href="../cerrar.php">Cerrar Sesion</a></li>
							</ul>
							<?php
						}else{
							?>	
							<button class= "boton" id="boton2" type="button"  onclick="window.location.href='../login-registro.php'">Log-in / Registro</button>
							<?php	
						}
					}else{
						?>	
						<button class= "boton" id="boton2" type="button"  onclick="window.location.href='../login-registro.php'">Log-in / Registro</button>
						<?php	
					}
					?>	             


				</div>

			</div>

			<div class="contenido" >
				<div class="ContenidoI">
					<h1>El juego</h1>
					<p>
					Beat Down es un juego que hereda el espíritu clásico de los arcade top-down por rondas, unshooter algo difícil al que te tendrás que acomodar antes de empezar a pasar rondas. Cada tres rondas tendrás un enemigo que te soltará un arma y munición para la misma. De igual modo, para ayudarte a pasar las rondas, los enemigos te irán soltando munición y salud que tendrás que recoger. Cada vez que superes una ronda podrás gastar puntos en subir tus atributos para que te sea más fácil superar las rondas siguientes. <br> <br>

						El juego está desarrollado íntegramente en javascript, apoyados en PIXI.js para el renderizado 2d en WebGL. Nuestros recursos pertenecen a OpenGameArt (menos un guiño a nuestro amado Terraria) y en el desarrollo se han visto involucradas otras librerías como Smoothie.js, sound.js, FPSMeter, y jQuery. <br> <br>

						Nos hemos inspirado en los clásicos y modernos, como el gran Hotline Miami, siendo quizá nuestro retoño una suerte de pequeño homenaje a la obra maestra de Dennaton Games, al amor por el código y al reconocimiento y respeto del trabajo de los estudios independientes. <br><br>

						¡Que nada te quite las ganas de crear! Esperamos que lo disfrutes. <br>

					</p>
				</div>
				<div class="footer">
					<p>Beat Down @ Copyright</p>
				</div>
			</div>
			<div class="banda_negras2"></div>
		</div>
	</body>
	</html>