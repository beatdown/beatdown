<!DOCTYPE html>
<html>
<head>
	<title>Juego</title>
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<meta charset="utf-8">
	<link rel="icon" href="../../img/dinoico.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="../../js/librerias/bootstrap-3.3.7-dist/css/bootstrap.css">
	<script type="text/javascript" src="../../js/librerias/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../../js/librerias/libreriaCookies.js"></script>
	<script type="text/javascript" src="../../js/librerias/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../cambiarPestanna.js"></script>
	<script type="text/javascript" src="../../js/librerias/fpsmeter.min.js"></script>
	<script type="text/javascript" src="../../js/librerias/smoothie.js"></script>
	<script type="text/javascript" src="../../js/librerias/soundjs-0.6.2.min.js"></script>
	<script type="text/javascript" src="../../js/librerias/pixi.min.js"></script>
	<script type="text/javascript" src="../../js/recursos/Utiles.js"></script>
	<script type="text/javascript" src="../../js/clases/ItemSuelo.js"></script>
	<script type="text/javascript" src="../../js/clases/Personaje.js"></script>
	<script type="text/javascript" src="../../js/clases/Enemigo.js"></script>
	<script type="text/javascript" src="../../js/clases/Jefe.js"></script>
	<script type="text/javascript" src="../../js/clases/Jugador.js"></script>
	<script type="text/javascript" src="../../js/clases/Arma.js"></script>
	<script type="text/javascript" src="../../js/clases/Pistola.js"></script>
	<script type="text/javascript" src="../../js/clases/PistolaJefe.js"></script>
	<script type="text/javascript" src="../../js/clases/Escopeta.js"></script>
	<script type="text/javascript" src="../../js/clases/Disparo.js"></script>
	<script type="text/javascript" src="../../js/recursos/TextosUI.js"></script>
    <script type="text/javascript" src="../../js/recursos/escenaInicio.js"></script>
    <script type="text/javascript" src="../../js/recursos/escenaJuego.js"></script>
	<script type="text/javascript" src="../../js/recursos/escenaFin.js"></script>
	<script type="text/javascript" src="../../js/recursos/asociacionTeclas.js"></script>
	<script type="text/javascript" src="../../js/recursos/keyboardHelper.js"></script>
	<script type="text/javascript" src="../../js/recursos/juego.js"></script>
	<script type="text/javascript" src="../../js/recursos/setup.js"></script>
	<script type="text/javascript" src="../../js/recursos/escenaNivel.js"></script>
	
</head>
<body>
	<div class="principal">
		<div class="banda_negras"></div>
		<div class="contenidoP">
			<div class="lb" onclick="inicio()">
				<div class="cabecera">
				</div>
			</div>
			<div class="menu">
				<div class="caja_boton dropdown">
					<button class= "boton" id="menu1" type="button" data-toggle="dropdown">Acerca de nosotros
					</button>
					<ul class="dropdown-menu " role="menu" aria-labelledby="menu1">
						<li role="presentation"><a role="menuitem" tabindex="-1" href="../../index.php">Quienes somos</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="../nosotros-des/eljuego.php">El juego</a></li>  
						<li role="presentation"><a role="menuitem" tabindex="-1" href="../nosotros-des/repositorio.php">Repositorio</a></li>    
					</ul>
				</div>
				<div class="caja_boton  dropdown">
					<button class= "boton" id="menu1" type="button" data-toggle="dropdown">Jugar
					</button>
					<ul class="dropdown-menu caja_menu" role="menu2" aria-labelledby="menu2">
						<li role="presentation"><a role="menuitem" tabindex="-2" href="juego.php">Juego</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-2" href="ranking.php">Ranking</a></li>
					</ul>
				</div>
				<div class="caja_boton dropdown">

					<?php
					if(isset($_COOKIE['inicio'])){
						
						if ($_COOKIE['inicio']==1){
							?>
							<button class= "boton" id="boton1" type="button" data-toggle="dropdown">Bienvenido <?php echo $_COOKIE['name']; ?> </button>
							<ul class="dropdown-menu caja_menu" role="menu2" aria-labelledby="menu2">
								<li role="presentation"><a role="menuitem" tabindex="-2" href="../cerrar.php">Cerrar Sesion</a></li>
							</ul>
							<?php
						}else{
							?>	
							<button class= "boton" id="boton2" type="button"  onclick="window.location.href='../login-registro.php'">Log-in / Registro</button>
							<?php	
						}
					}else{
						?>	
						<button class= "boton" id="boton2" type="button"  onclick="window.location.href='../login-registro.php'">Log-in / Registro</button>
						<?php	
					}
					?>	             


				</div>

			</div>

			<div class="contenido" >
				<?php
				if(isset($_COOKIE['inicio'])){
					if ($_COOKIE['inicio']==1){
						?>
						<div class="contenidoI bordeJ" id="juego">
						</div>
						<?php
					}else{
						?>	
						<div class="">
							<h3>Por favor para poder jugar a este juego se necesita estar logueado , pulse en la pestaña de arriba a la derecha o en el siguiente <a href="../login-registro.php" >enlace </a></h3>
						</div>
						<?php	
					}
				}else{
					?>	
					<div class="">
						<h3>Por favor para poder jugar a este juego se necesita estar logueado , pulse en la pestaña de arriba a la derecha o en el siguiente <a href="../login-registro.php" >enlace </a></h3>
					</div>
					<?php	

				}
				?>
			</div>
			<div class="footer">
				<p>Beat Down @ Copyright</p>
			</div>
		</div>
		<div class="banda_negras2"></div>
	</div>
</body>
</html>