// Inicialización de las variables necesarias para el correcto funcionamiento de escenaNivel
var escenanivel, mensajenivel, mensajepuntos, mensajesubir1, mensajesubir2, mensajesubir3, mensajesubir4, mensajesubir5, vida, velocidad, resistencia, penetracion, velocidadataque;

/**
 * Inicialización de la escena,textos y funcionalidad encargada de subir atributos a nuestro personaje.
 */
function inicializarEscenaNivel() {
    escenanivel = new PIXI.Container();
    stage.addChild(escenanivel);

    // Estilo que le damos al texto base de la escena.
    var style = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 36,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#000000',
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6
    });
    
    // Estilo que le damos al texto al pasar el ratón por encima.
    var style2 = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 40,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#FFFFFF',
        dropShadowBlur: 8,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 8
    });

    // Estilos que le ponemos a los simbolos +.
    var style3 = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 60,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#000000',
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6
    });

    // Estilos que le ponemos a los simbolos + cuando pasamos el ratón por encima.
    var style3b = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 60,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#FFFFFF',
        dropShadowBlur: 8,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 8
    });

    // Estilo que le asignamos a uno de los texto de la escena.
    var style4 = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 30,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#000000',
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6
    });
    
    // Declaramos la variable en la que guardamos el mensaje, y le asignamos una posición al mismo. Ademas de hacer interactivo uno de los textos para asignarle funcionalidades.
    mensajenivel = new PIXI.Text("¡NIVEL " + nivel + " SUPERADO!", style);
    mensajenivel.x = 300;
    mensajenivel.y = 45;

    escenanivel.addChild(mensajenivel);

    mensajepuntos = new PIXI.Text("Has ganado " + puntos + " punto para gastar en tus habilidades", style4);
    mensajepuntos.x = 120;
    mensajepuntos.y = 110;

    escenanivel.addChild(mensajepuntos);

    mensajesubir1 = new PIXI.Text("+", style3);
    mensajesubir1.x = 400;
    mensajesubir1.y = 150;
    mensajesubir1.interactive = true;
    mensajesubir1.buttonMode = true;
    
    // Asignamos  funcionalidad al texto
    mensajesubir1

        .on('mousedown', onButtonDown1)
        .on('mouseover', onButtonOver1)
        .on('mouseout', onButtonOut1);
    
    /**
     * Cuando pulsamos en el texto, subiremos un atributo determinado y actualizaremos los puntos que nos quedan para gastar.
     */
    function onButtonDown1() {
        if (puntos > 0) {
            jugador.vidaMax += 10;
            jugador.vida = jugador.vidaMax;
            escenanivel.removeChild(mensajepuntos);
            puntos--;
            mensajepuntos = new PIXI.Text("Has ganado " + puntos + " punto para gastar en tus habilidades", style4);
            mensajepuntos.x = 120;
            mensajepuntos.y = 110;
            escenanivel.addChild(mensajepuntos);
            //console.log(puntos);
        }
    } // Fin onButtonDown1
    
    /**
     * Si pasamos el ratón por encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOver1() {
        mensajesubir1.style = style3b;
    } // Fin onButtonOver1
    
    /**
     * Si quitamos el ratón de encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOut1() {
        mensajesubir1.style = style3;
    } // Fin onButtonOut1

    escenanivel.addChild(mensajesubir1);

    /**
     * Declaramos variables donde guardar los textos que pondremos en la escena y le asignamos funcionalidades.
     */
    vida = new PIXI.Text("Vida", style);
    vida.x = 450;
    vida.y = 165;

    escenanivel.addChild(vida);

    mensajesubir2 = new PIXI.Text("+", style3);
    mensajesubir2.x = 400;
    mensajesubir2.y = 200;
    mensajesubir2.interactive = true;
    mensajesubir2.buttonMode = true;
    // Asignamos  funcionalidad al texto
    mensajesubir2

        .on('mousedown', onButtonDown2)
        .on('mouseover', onButtonOver2)
        .on('mouseout', onButtonOut2);
    
    /**
     * Al pulsar en el texto en concreto nos subira un atributo en concreto y actualizara los puntos que nos quedan por gastar
     */
    function onButtonDown2() {
        if (puntos > 0) {
            jugador.velocidad *= 1.05;
            escenanivel.removeChild(mensajepuntos);
            puntos--;
            mensajepuntos = new PIXI.Text("Has ganado " + puntos + " punto para gastar en tus habilidades", style4);
            mensajepuntos.x = 120;
            mensajepuntos.y = 110;
            escenanivel.addChild(mensajepuntos);
            //console.log(puntos);
        }
    }
    /**
     * Si pasamos el ratón por encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOver2() {
        mensajesubir2.style = style3b;
    }
     /**
     * Si quitamos el ratón de encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOut2() {
        mensajesubir2.style = style3;
    }

    escenanivel.addChild(mensajesubir2);
    /**
     * Declaramos variables donde guardar los textos que pondremos en la escena y le asignamos funcionalidades.
     */
    velocidad = new PIXI.Text("Velocidad", style);
    velocidad.x = 450;
    velocidad.y = 215;

    escenanivel.addChild(velocidad);

    mensajesubir3 = new PIXI.Text("+", style3);
    mensajesubir3.x = 400;
    mensajesubir3.y = 250;
    mensajesubir3.interactive = true;
    mensajesubir3.buttonMode = true;
    // Asignamos  funcionalidad al texto
    mensajesubir3

        .on('mousedown', onButtonDown3)
        .on('mouseover', onButtonOver3)
        .on('mouseout', onButtonOut3);
    
    /**
     * Al pulsar en el texto en concreto nos subira un atributo en concreto y actualizara los puntos que nos quedan por gastar
     */
    function onButtonDown3() {
        if (puntos > 0) {
            jugador.resistencia *= 1.05;
            escenanivel.removeChild(mensajepuntos);
            puntos--;
            mensajepuntos = new PIXI.Text("Has ganado " + puntos + " punto para gastar en tus habilidades", style4);
            mensajepuntos.x = 120;
            mensajepuntos.y = 110;
            escenanivel.addChild(mensajepuntos);
            //console.log(puntos);
        }
    }
    /**
     * Si pasamos el ratón por encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOver3() {
        mensajesubir3.style = style3b;
    }
     /**
     * Si quitamos el ratón de encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOut3() {
        mensajesubir3.style = style3;
    }

    // 
    escenanivel.addChild(mensajesubir3);
    /**
     * Declaramos variables donde guardar los textos que pondremos en la escena y le asignamos funcionalidades.
     */
    resistencia = new PIXI.Text("Resistencia", style);
    resistencia.x = 450;
    resistencia.y = 260;

    escenanivel.addChild(resistencia);

    mensajesubir4 = new PIXI.Text("+", style3);
    mensajesubir4.x = 400;
    mensajesubir4.y = 300;
    mensajesubir4.interactive = true;
    mensajesubir4.buttonMode = true;
    
    // Asignamos  funcionalidad al texto
    mensajesubir4

        .on('mousedown', onButtonDown4)
        .on('mouseover', onButtonOver4)
        .on('mouseout', onButtonOut4);
    
    /**
     * Al pulsar en el texto en concreto nos subira un atributo en concreto y actualizara los puntos que nos quedan por gastar
     */
    function onButtonDown4() {
        if (puntos > 0) {
            jugador.penetracion *= 1.15;
            escenanivel.removeChild(mensajepuntos);
            puntos--;
            mensajepuntos = new PIXI.Text("Has ganado " + puntos + " punto para gastar en tus habilidades", style4);
            mensajepuntos.x = 120;
            mensajepuntos.y = 110;
            escenanivel.addChild(mensajepuntos);
            //console.log(puntos);
        }
    }
    
    /**
     * Si pasamos el ratón por encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOver4() {
        mensajesubir4.style = style3b;
    }

    /**
     * Si quitamos el ratón de encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOut4() {
        mensajesubir4.style = style3;
    }
    
    
    escenanivel.addChild(mensajesubir4);
    /**
     * Declaramos variables donde guardar los textos que pondremos en la escena y le asignamos funcionalidades.
     */
    penetracion = new PIXI.Text("Penetración", style);
    penetracion.x = 450;
    penetracion.y = 315;

    escenanivel.addChild(penetracion);

    mensajenivel2 = new PIXI.Text("Continuar", style);
    mensajenivel2.x = 380;
    mensajenivel2.y = 480;
    mensajenivel2.interactive = true;
    mensajenivel2.buttonMode = true;

    // Asignamos  funcionalidad al texto
    mensajenivel2

        .on('mousedown', onButtonDown)
        .on('mouseover', onButtonOver)
        .on('mouseout', onButtonOut);

    escenanivel.visible = true;

     /**
     * Al pulsar en el texto en concreto nos subira un atributo en concreto y actualizara los puntos que nos quedan por gastar
     */
    function onButtonDown() {
        if (puntos == 0) {
            puntos = 4;
            nivel++;

            inicializarEscenaJuego();

            escenanivel.visible = false;
            stage.removeChild(this);

        }

    }
    
     /**
     * Si pasamos el ratón por encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOver() {
        mensajenivel2.style = style2;
    }
    
     /**
     * Si quitamos el ratón de encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOut() {
        mensajenivel2.style = style;
    }

    // 
    escenanivel.addChild(mensajenivel2);

}  //Fin inicializarEscenaNivel
