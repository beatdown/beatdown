// ESTAS VARIABLES SON DECLARADAS AQUÍ DEBIDO AL ALCANCE DE LA VISIBILIDAD EN JAVASCRIPT


// Variable de alcance global usada para el medidor de FPS. En teoría estará sólo durante la fase de desarrollo.
var meter;

// Alias PIXI
var Container = PIXI.Container,
    autoDetectRenderer = PIXI.autoDetectRenderer,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    TextureCache = PIXI.utils.TextureCache,
    Texture = PIXI.Texture,
    Sprite = PIXI.Sprite,
    Text = PIXI.Text,
    Graphics = PIXI.Graphics,
    Style = PIXI.TextStyle;

// Constante VELOCIDAD con la que se ajustan varios parámetros del juego, como la velocidad de los enemigos o del jugador.
const VELOCIDAD = 1;

// Los puntos de atributo que el jugador puede subir entre rondas.
var puntos = 4;

// Variable de control de niveles, empieza en uno por una cuestión conservadora del mundo de los videojuegos, vaya.
var nivel = 1;

// Variable de control de la puntuacion global . Cada enemigo nos dará 1 punto, que se multiplicara por el nivel en el que estemos.
var puntuacion = 0;

// Variable de control del total de enemigos derrotados a lo largo del juego. Servirá como factor diferenciador en la puntuación final.
var totalEnemigosMuertos = 0;

// Enemigos muertos por RONDA.
var enemigosMuertos = 0;

// Variable que almacena los enemigos a derrotar en cada ronda. Este valor va aumentando según se avanza de nivel.
var enemigosAMatar = 6;

// Variable de control para detener la generación de enemigos.
var generarEnemigos = true;

// Variable donde almacenamos los objetos de tipo Disparo para llevar un control sobre los mismos y poder iterar sobre ellos.
var disparos = [];

//Variable donde se almacenan los objetos de tipo Disparo del jefe
var disparosEnemigos = [];

// Variable donde almacenamos los objetos de tipo Enemigo para llevar un control sobre los mismos y poder iterar sobre ellos.
var enemigos = [];

// Variable donde se almacena el objeto de tipo Jugador, que es el personaje manejado por el humano y que juega.
var jugador;

// Variable booleana de control para ejecutar ciertas acciones si el enemigo está sufriendo daño en ese fotograma.
var jugadorGolpeado;

// PIXI.Container donde se cargan los mapas. Posteriormente se vuelca al container escenaJuego.
var escenario;

// Variable de control para controlar el estado del juego (start | stop).
var state;

// Variable que controla cuándo se ha acabado nuestra partida.
var fin;

// Variable que almacena el objeto de tipo Pistola.
var pistola;

// Variable que almacena el objeto de tipo Escopeta.
var escopeta;

// Variable que almacena los enemigos que luego son pusheados al array enemigos[].
var en;

// Array donde guardamos las texturas de los mapas disponibles para ser cargados en el escenario.
var mapasDisponibles = [];

// Array donde guardamos las dimensiones y contains de los mapas
var limitesMapas = [];

// Variable donde se guarda el valor aleatorio entero para elección de mapa
var generarMapa;

// Array donde guardamos las texturas de los jefes disponibles para ser cargados a la hora de generar un jefe nuevo.
var jefesDisponibles = [];

// Array donde guardamos las armas disponibles para ser dropeadas por un jefe.
var armasDisponibles = [];

// Variable de control de si hay un jefe ahora mismo activo en la ronda o no.
var hayJefe = false;

// Cambiar de nivel
var cambiarDeNivel=false;

// Variable donde se alamcena nuestro objeto de tipo Jefe, que será creado en la ronda correspondiente.
var jefe;

// Variable que controla cada cuántas rondas queremos que spawnee un jefe.
var jefeCadaRonda = 3;

// Array donde se almacenan todos los items que caen al suelo por parte de los enemigos.
var itemsSuelo = [];

// Variables donde almacenamos los sonidos, bastante explícito qué almacena cada uno.
var soundID = "disparo";
var soundID2 = "daño";
var soundID4 = "melodia";
var soundID5 = "derrota";
var soundID6 = "muerte";

// Container donde se vuelca todo el demás contenido de PIXI.
var stage = new Container();

// El renderer donde se renderizará el juego.
var renderer = autoDetectRenderer(960, 545);

// Container con el contenido de las rondas.
var escenaJuego = new Container();
escenaJuego.visible = false;
stage.addChild(escenaJuego);

// Objeto de tipo Smoothie (librería externa) para controlar las llamadas al playLoop, así como el menú PAUSA.
var smoothie = new Smoothie({
    engine: PIXI,
    renderer: renderer,
    root: stage,

    /**
     * Modificar el atributo fps altera las funcionalidades del juego, ya que los controla la cantidad de veces que se llamará al playLoop por segundo. 
     * Si por ejemplo lo ponemos a 30 el juego se verá el doble de lento, sin embargo las animaciones seguirán igual de fluidas gracias a las funcionalidades de Smoothie.
     */
    fps: 60,
    update: gameLoop.bind(this)
});


document.onreadystatechange = function () {
    if (document.readyState === "complete") {
        cargarSetup();
    }
};

/**
 * Carga PIXI, las texturas y llama a la inicialización de los objetos dentro de PIXI, así como todos los elementos del juego.
 */
function cargarSetup() {

    // Objeto medidor de FPS. Debug mode.
    meter = new FPSMeter(document.body, {
        theme: 'dark',
        heat: 1,
        graph: 1
    });

    // Se agrega el renderizador.
    document.getElementById("juego").appendChild(renderer.view);

    // Carga de las texturas necesarias.
    PIXI.loader
        .add("img/Top_Down_Survivor/shotgun/idle/survivor-idle_shotgun_0.png")
        .add("img/health.png")
        .add("img/ammo.png")
        .add("img/personaje.png")
        .add("img/shot_gun.png")
        .add("img/map.png")
        .add("img/map_metal.png")
        .add("img/map_nieve.png")
        .add("img/map_nieve1.png")
        .add("img/map2.png")
        .add("img/enemigo.png")
        .add("img/ojo1.png")
        .add("img/ojo2.png")
        .add("img/disparo.png")
        .load(setup);

    // Carga de los sonidos necesarios
    createjs.Sound.registerSound("../../sonidos/disparo.wav", soundID);
    //createjs.Sound.registerSound("../../sonidos/daño.wav", soundID2);
    createjs.Sound.registerSound("../../sonidos/Hypnotic-Puzzle3.mp3", soundID4);
    createjs.Sound.registerSound("../../sonidos/derrota.mp3", soundID5);
    createjs.Sound.registerSound("../../sonidos/muerte.wav", soundID6);



    /**
     * Carga los elementos de juego, genera los sprites y crea los objetos e inicializa las escenas.
     */
    function setup() {

        // Empuja a sus arrays correspondientes los mapas, dimensiones y los jefes.
        mapasDisponibles.push(resources["img/map.png"].texture);
        mapasDisponibles.push(resources["img/map_metal.png"].texture);
        mapasDisponibles.push(resources["img/map_nieve.png"].texture);
        mapasDisponibles.push(resources["img/map_nieve1.png"].texture);
        mapasDisponibles.push(resources["img/map2.png"].texture);

        limitesMapas.push({
            x: 47,
            y: 97,
            width: 960,
            height: 545
        });
        
        limitesMapas.push({
            x: 48,
            y: 108,
            width: 960,
            height: 545
        });
        
        limitesMapas.push({
            x: 49,
            y: 58,
            width: 960,
            height: 545
        });
        
        limitesMapas.push({
            x: 49,
            y: 58,
            width: 950,
            height: 545
        });
        
        limitesMapas.push({
            x: 45,
            y: 45,
            width: 960,
            height: 545
        });


        jefesDisponibles.push(resources["img/enemigo.png"].texture);
        jefesDisponibles.push(resources["img/ojo1.png"].texture);
        jefesDisponibles.push(resources["img/ojo2.png"].texture);


        // Alias
        var texturaJugadorPistola = resources["img/personaje.png"].texture;
        var texturaJugadorEscopeta = resources["img/Top_Down_Survivor/shotgun/idle/survivor-idle_shotgun_0.png"].texture;
        var texturaEscopeta = resources["img/shot_gun.png"].texture;
        var texturaDisparoPistola = resources["img/disparo.png"].texture;
        var texturaMunicion = resources["img/ammo.png"].texture;
        var texturaSalud = resources["img/health.png"].texture;

        // Creación del jugador y asignación de su multiplicador de velocidad y de sus atributos iniciales.
        jugador = new Jugador(texturaJugadorPistola, 100, 100, 1, 1, 1, 1, readCookie("name"));
        jugador.velocidad = VELOCIDAD * 3;

        jugador.y = 96;
        jugador.x = 96;
        jugador.vx = 0;
        jugador.vy = 0;
        jugador.scale.set(0.25, 0.25);
        jugador.anchor.set(0.5, 0.5);


        // Creación de las armas y asociación de las mismas a jugador y armasDisponibles[]
        pistola = new Pistola(texturaJugadorPistola, texturaDisparoPistola, "Pistola \"Lola\"", "Pistola", 20, 10, 10, "∞");
        jugador.armas[0] = pistola;

        escopeta = new Escopeta(texturaJugadorEscopeta, texturaDisparoPistola, "Escopeta \"Makapeta\"", "Escopeta", 50, 10, 10, 0, texturaEscopeta);
        armasDisponibles.push(escopeta);


        // Inicializar escenaJuego (donde se desarrollan las rondas, vaya).
        inicializarEscenaJuego();

        // Inicializar escena inicio.
        inicializarEscenaInicio();


        // Inicializar escena final, sólo mostrada cuando termine la partida.
        inicializarEscenaFin();


        // Llamada a la función de asociación de teclas.
        asociarTeclas();


        // Inicialización del playLoop.
        smoothie.start();
    }
}
