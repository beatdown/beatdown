// Variables necesarias para el funcionamiento de la escena inicial
var escenainicio,mensajeinicio,mensajeinicio2,instance,mensajetutorial,arriba,izq,der,abajo,disparar,cArma,pausa;

/**
 * Función encargade de inicializar la escena de inicio, con la declaración de los estilos
 * a usar, agregado de los mensajes al container, así como los listener de los botones.
 */
function inicializarEscenaInicio(){
    //instance.volume = 0.5;
    escenaJuego.visible= false;
    escenainicio = new PIXI.Container();
    stage.addChild(escenainicio);
    // Estilo que le aplicamos al texto base de la escena.
    var style = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 36,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#000000',
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6
    });
    // Estilo que le aplicamos al texto cuando pasamos el ratón por encima del mismo.
    var style2 = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 40,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#FFFFFF',
        dropShadowBlur: 8,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 8
    });
    // Estilo que le aplicamos a una parte del texto de la escena.
     var style3 = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 28,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#000000',
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6
    });
    // Declaración de la variable donde se guardara texto de la escena y su posición.
    mensajeinicio = new PIXI.Text("Bienvenido a Beat Down, "+jugador.nombre,style);

    mensajeinicio.x = 220;
    mensajeinicio.y= 25;

    escenainicio.addChild(mensajeinicio);
    
    // Declaración de la variable donde se guardara texto de la escena y su posición.
    mensajetutorial = new PIXI.Text("Para moverte, utiliza : ",style3);
    mensajetutorial.x = 310;
    mensajetutorial.y = 75;
    
    escenainicio.addChild(mensajetutorial);
    
    // Declaración de la variable donde se guardara texto de la escena y su posición.
    arriba = new PIXI.Text(" w  -> Arriba",style3);
    arriba.x = 340;
    arriba.y = 105;
    
    escenainicio.addChild(arriba);
    
    // Declaración de la variable donde se guardara texto de la escena y su posición.
    abajo = new PIXI.Text(" s  -> Abajo",style3);
    abajo.x = 340;
    abajo.y = 135;
    
    escenainicio.addChild(abajo);
    
    // Declaración de la variable donde se guardara texto de la escena y su posición.
    izq = new PIXI.Text(" a  -> Izquierda",style3);
    izq.x = 340;
    izq.y = 165;
    
    escenainicio.addChild(izq);
    
    // Declaración de la variable donde se guardara texto de la escena y su posición.
    der = new PIXI.Text(" d  -> Derecha",style3);
    der.x = 340;
    der.y = 195;
    
    escenainicio.addChild(der);
    
    // Declaración de la variable donde se guardara texto de la escena y su posición.
    disparar = new PIXI.Text("Para disparar, click izquierdo del ratón",style3);
    disparar.x = 175;
    disparar.y = 240;
    
    escenainicio.addChild(disparar);
    
    // Declaración de la variable donde se guardara texto de la escena y su posición.
    cArma = new PIXI.Text("Para cambiar de arma, la tecla espacio",style3);
    cArma.x = 175;
    cArma.y = 275;
    
    escenainicio.addChild(cArma);
    
    // Declaración de la variable donde se guardara texto de la escena y su posición.
    pausa = new PIXI.Text("Para pausar la partida, la tecla p",style3);
    pausa.x = 175;
    pausa.y = 310;
    
    escenainicio.addChild(pausa);
    
    // Declaración de la variable donde se guardara texto de la escena y su posición, y la funcionalidad de que sea interactivo.
    mensajeinicio2 = new PIXI.Text("Haz click para jugar...",style);

    mensajeinicio2.x = 275;
    mensajeinicio2.y= 450;
    mensajeinicio2.interactive = true;
    mensajeinicio2.buttonMode= true;
    
    // Asignamos  funcionalidad al texto
    mensajeinicio2

    .on('mousedown', onButtonDown)
    .on('mouseover', onButtonOver)
    .on('mouseout', onButtonOut);
    
    /**
     * Cuando pulsamos en el texto, cambiaremos entre las escenas que tenemos, y añadiremos la musica de fondo del videojuego.
     */
    function onButtonDown() {
        escenainicio.visible = false;
        escenaJuego.visible = true;
        var myInstance = createjs.Sound.play(soundID4,{loop:-1});
        myInstance.volume = 0.25;
    }// Fin onButtonDown
    /**
     * Si pasamos el ratón por encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOver() {
        mensajeinicio2.style=style2;
    }// Fin onButtonOver

     /**
     * Si quitamos el ratón de encima del texto, cambiara el estilo que le asginamos a dicho texto.
     */
    function onButtonOut() {
        mensajeinicio2.style=style;
    }// Fin onButtonOut
    
    escenainicio.addChild(mensajeinicio2);   
}
