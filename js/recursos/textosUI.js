//Variables con alcance global para su posterior tratamiento.
var textoArma, textoMunicion, textoEnemigos, textoContadorEnemigos, textoNivel, textoContadorNivel, textoSalud, textoContadorSalud;

//Variable con el estilo de texto grande.
var estiloTextoGrande = new PIXI.TextStyle({
    fontFamily: 'Impact',
    fontSize: 32,
    fontStyle: 'italic',
    fontWeight: 'bold',
    fill: ['#ffffff', '#00ff99'], // gradiente
    stroke: '#4a1850',
    strokeThickness: 5,
    dropShadow: true,
    dropShadowColor: '#000000',
    dropShadowBlur: 4,
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 6,

});

//Variable con el estilo de texto pequeño.
var estiloTextoPequeño = new PIXI.TextStyle({
    fontFamily: 'Arial',
    fontSize: 26,
    fontStyle: 'italic',
    fontWeight: 'bold',
    fill: ['#ffffff', '#00ff99'], // gradiente
    stroke: '#4a1850',
    strokeThickness: 5,
    dropShadow: true,
    dropShadowColor: '#000000',
    dropShadowBlur: 4,
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 6,

});

//Variable con el estilo del texto de la salud. Es una función aparte debido a que esta variable es modificada por la función actualizarVida() y modificaría la estética de los otros textos.
var estiloSalud = new PIXI.TextStyle({
    fontFamily: 'Arial',
    fontSize: 26,
    fontStyle: 'italic',
    fontWeight: 'bold',
    fill: ['#ffffff', '#00ff99'], // gradiente
    stroke: '#4a1850',
    strokeThickness: 5,
    dropShadow: true,
    dropShadowColor: '#000000',
    dropShadowBlur: 4,
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 6,

});

/** Clase de métodos estáticos para mostrar la interfaz de textos del juego. */
class TextosUI {
    
    /**
     * Este método crea variables para mostrar nuestra arma seleccionada en el momento de la llamada así como la munición disponible.
     */
    static inicializarTextosMunicion() {

        textoArma = new PIXI.Text(jugador.armas[jugador.arma_seleccionada].nombre, estiloTextoGrande);
        textoArma.x = 30;
        textoArma.y = 5;


        textoMunicion = new PIXI.Text(jugador.armas[jugador.arma_seleccionada].municion, estiloTextoPequeño);
        textoMunicion.x = 30;
        textoMunicion.y = 45;

        escenaJuego.addChild(textoArma);
        escenaJuego.addChild(textoMunicion);

    }
    
    /**
     * Este método crea variables para mostrar "Enemigos" y la cantidad eliminada de los mismos en esta ronda en el momento de la llamada.
     */
    static inicializarTextosEnemigos() {
        textoEnemigos = new PIXI.Text("ENEMIGOS", estiloTextoGrande);
        textoEnemigos.x = 320;
        textoEnemigos.y = 5;

        textoContadorEnemigos = new PIXI.Text(enemigosMuertos, estiloTextoPequeño);
        textoContadorEnemigos.x = 320;
        textoContadorEnemigos.y = 45;

        escenaJuego.addChild(textoEnemigos);
        escenaJuego.addChild(textoContadorEnemigos);



    }

    /**
     * Este método crea variables para mostrar "Nivel" y el nivel en que nos encontramos en el momento de la llamada.
     */
    static inicializarTextosNivel() {
        textoNivel = new PIXI.Text("NIVEL", estiloTextoGrande);
        textoNivel.x = 600;
        textoNivel.y = 5;

        textoContadorNivel = new PIXI.Text(nivel, estiloTextoPequeño);
        textoContadorNivel.x = 600;
        textoContadorNivel.y = 35;

        escenaJuego.addChild(textoNivel);
        escenaJuego.addChild(textoContadorNivel);
    }

    /**
     * Este método crea variables para mostrar "SALUD" y la cantidad de salud en esta ronda en el momento de la llamada.
     */
    static inicializarTextosSalud() {
        textoSalud = new PIXI.Text("SALUD", estiloTextoGrande);
        textoSalud.x = 800;
        textoSalud.y = 5;

        textoContadorSalud = new PIXI.Text(parseInt(jugador.vida), estiloSalud);
        textoContadorSalud.x = 800;
        textoContadorSalud.y = 35;

        escenaJuego.addChild(textoSalud);
        escenaJuego.addChild(textoContadorSalud);
    }


    /**
     * Método simple para mostrar la munición del arma seleccionada en el momento de la llamada.
     */
    static actualizarMunicion() {
        textoMunicion.text = jugador.armas[jugador.arma_seleccionada].municion;
    }

    /**
     * Método simple para mostrar el arma seleccionada en el momento de la llamada.
     */
    static actualizarArma() {
        textoArma.text = jugador.armas[jugador.arma_seleccionada].nombre;
    }
    
    /**
     * Método simple para mostrar el número de enemigos matados en esta ronda en el momento de la llamada.
     */
    static actualizarEnemigos() {
        textoContadorEnemigos.text = enemigosMuertos;
    }
    
    /**
     * Método simple para mostrar el nivel en que nos encontramos en el momento de la llamada.
     */
    static actualizarNivel() {
        textoContadorNivel.text = nivel;
    }

    /**
     * Método simple para mostrar la cantidad de vida restante en el momento de la llamada. Si la salud es inferior a 1/5 de la vida total que puede tener el jugador, se muestra en rojo.
     */
    static actualizarVida() {
        textoContadorSalud.text = parseInt(jugador.vida);
        if (jugador.vida > (jugador.vidaMax / 5)) {
            textoContadorSalud.style.fill = ['#ffffff', '#00ff99'];
        } else {
            textoContadorSalud.style.fill = ['#ffffff', '#ff0000'];
        }
    }

    /**
     * Método para inicializar todos los textos de la interfaz.
     */
    static inicializarTextos() {
        TextosUI.inicializarTextosMunicion();
        TextosUI.inicializarTextosNivel();
        TextosUI.inicializarTextosEnemigos();
        TextosUI.inicializarTextosSalud();
    }

}
