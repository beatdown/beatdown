/** Clase de métodos estáticos para aunar las funciones necesarias para el funcionamiento del juego. */
class Utiles {
    
    /**
     * Recorre el array de items dropeados en el suelo y comprueba si algún item colisiona con el jugador. 
     * Dependiendo del tipo de item se realiza una u otra acción (recoger salud, recoger munición y asociarla al arma, recoger un arma y asociarla al jugador). 
     * Tras ello actualiza la interfaz.
     */
    static checkItemsSuelo() {
        itemsSuelo.forEach(function (item) {
            if (Utiles.checkColision(item, jugador)) {
                switch (item.tipo) {
                    case "salud":
                        jugador.vida += item.atributo;
                        if (jugador.vida > jugador.vidaMax) {
                            jugador.vida = jugador.vidaMax;
                        }
                        TextosUI.actualizarVida();

                        break;

                    case "municion":
                        if (jugador.armas[1] != null) {
                            jugador.armas[1].municion += item.atributo;
                            TextosUI.actualizarMunicion();
                        }

                        break;

                    case "arma":
                        jugador.armas[1] = armasDisponibles[item.atributo];
                        jugador.armas[1].municion += 100;
                        break;
                    default:
                        break;
                }

                escenaJuego.removeChild(item);
                itemsSuelo.splice(itemsSuelo.indexOf(item), 1);

            }
        });
    } // Fin checkItemsSuelo
    
    /**
     * Itera sobre el array disparosEnemigos y actualiza sus posiciones. Comprueba colisiones con jugador y actualiza estadísticas e interfaz en caso de colisión.
     * Itera sobre el array de disparos y actualiza sus posiciones.
     * Por cada elemento dentro de disparos itera sobre el array de enemigos y comprueba las colisiones. En caso de contacto, actualiza las estadísticas de los enemigos.
     * Cada vez que un disparo impacta contra algo o sale del mapa, el disparo es eliminado del array y de escenaJuego.
     */
    static checkDisparos() {
        
        // Comprobación de disparos de los enemigos
        disparosEnemigos.forEach(function (disparo) {
            disparo.position.x += Math.cos(disparo.rotation) * disparo.velocidad;
            disparo.position.y += Math.sin(disparo.rotation) * disparo.velocidad;

            if (Utiles.contain(disparo, escenaJuego) != "null") {
                escenaJuego.removeChild(disparo);
                disparosEnemigos.splice(disparos.indexOf(disparo), 1);
            }


            if (Utiles.checkColision(disparo, jugador)) {
                jugador.recibirDaño(disparo.daño * jefe.penetracion);
                escenaJuego.removeChild(disparo);
                TextosUI.actualizarVida();
                disparosEnemigos.splice(disparos.indexOf(disparo), 1);
            }
        });
        
        // Comprobación de los disparos del jugador
        disparos.forEach(function (disparo) {
            disparo.position.x += Math.cos(disparo.rotation) * disparo.velocidad;
            disparo.position.y += Math.sin(disparo.rotation) * disparo.velocidad;

            if (Utiles.contain(disparo, escenaJuego) != "null") {
                escenaJuego.removeChild(disparo);
                disparos.splice(disparos.indexOf(disparo), 1);
            }

            if (hayJefe) {
                if (Utiles.checkColision(disparo, jefe)) {
                    jefe.disparar();
                    jefe.recibirDaño(disparo.daño * jugador.penetracion);
                    escenaJuego.removeChild(disparo);
                    disparos.splice(disparos.indexOf(disparo), 1);
                }
            }

            enemigos.forEach(function (enemigo) {
                if (Utiles.checkColision(disparo, enemigo)) {
                    enemigo.recibirDaño(disparo.daño * jugador.penetracion);
                    escenaJuego.removeChild(disparo);
                    disparos.splice(disparos.indexOf(disparo), 1);
                }
            });



        });
    } // Fin checkDisparos
    
    /**
     * Rota el jugador hacia la posición del ratón. Actualiza su posición.
     */
    static rotarJugador() {
        jugador.rotar((renderer.plugins.interaction.mouse.global));
        jugador.x += jugador.vx;
        jugador.y += jugador.vy;
    } // Fin rotarJugador
    
    /**
     * Itera sobre el array de enemigos, los rota hacia el jugador, comprueba colisiones y actualiza la interfaz si es necesario.
     * Si un enemigo está colisionando con un jugador, a jugador se le quitará vida con base en las estadísticas de ambos.
     * Si el jugador es golpeado, entrará en un estado de semitransparencia.
     * Si el enemigo se queda sin salud se aumenta el contador puntuacion, reproduce un sonido, llamada a la función drop del enemigo, se saca al enemigo de la escena del juego y de su array y se aumentan los contadores correspondientes de control.
     * En caso de ser un mapa con jefe, se aplican las mismas condiciones y funciones al mismo.
     * Alcanzado el total de enemigos a matar, se cambia de nivel.
     * Si el jugador se queda sin vida, se acaba la partida.
     */
    static checkEnemigos() {
        
        // Código que sólo se ejecuta si es un mapa con jefe.
        if (hayJefe) {
            jefe.moverEnemigo();
            jefe.rotar(jugador.position);
            if (Utiles.checkColision(jefe, jugador)) {
                jugador.alpha = 0.5;
                jugador.recibirDaño(jefe.dañoEnemigo * jefe.penetracion);
                TextosUI.actualizarVida();

            } else {
                jugador.alpha = 1;

            }

            if (jefe.vida <= 0) {
                jefe.drop();
                escenaJuego.removeChild(jefe);
                hayJefe = false;

            }
        }
        
        // Iteración sobre enemigos
        enemigos.forEach(function (enemigo) {
            enemigo.moverEnemigo();
            enemigo.rotar(jugador.position);

            if (Utiles.checkColision(jugador, enemigo)) {
                jugadorGolpeado = true;
            } else {
                jugadorGolpeado = false;
            }

            if (jugadorGolpeado) {
                jugador.alpha = 0.5;
                jugador.recibirDaño(enemigo.dañoEnemigo * (enemigo.penetracion / jugador.resistencia));
                //Sonido
                //createjs.Sound.play(soundID2);

                TextosUI.actualizarVida();

            } else {

                //Make the explorer fully opaque (non-transparent) if it hasn't been hit
                jugador.alpha = 1;
            }
            
            // Comprobar salud enemigo y acciones en consecuencia
            if (enemigo.vida <= 0) {
                puntuacion += (1 * nivel);
                createjs.Sound.play(soundID6);
                enemigo.drop();

                escenaJuego.removeChild(enemigo);

                enemigos.splice(enemigos.indexOf(enemigo), 1);
                totalEnemigosMuertos++;
                enemigosMuertos++;
                TextosUI.actualizarEnemigos();
            }


        });
        
        // Check si ha terminado la ronda
        if (enemigosMuertos >= enemigosAMatar && !hayJefe && !cambiarDeNivel) {
            cambiarDeNivel = true;
            Utiles.cambiarDeNivel();
        }

        // Check jugador muerto
        if (jugador.vida <= 0) {
            createjs.Sound.play(soundID5);
            actualizarPuntuacion();
            state = end;

        }
    } // Fin checkEnemigos


    /**
     * Clase que se encarga de encapsular un sprite en un container
     * @param   {object} sprite    sprite
     * @param   {object} container contenedor
     * @returns {string} colision   por qué lado colisiona y se contiene.
     */
    static contain(sprite, container) {

        var colision = "null";

        // Izq
        if (sprite.x < container.x) {
            sprite.x = container.x;
            colision = "left";
        }

        // Arriba
        if (sprite.y < container.y) {
            sprite.y = container.y;
            colision = "top";
        }

        // Dcha
        if (sprite.x + sprite.width > container.width) {
            sprite.x = container.width - sprite.width;
            colision = "right";
        }

        // Abajo
        if (sprite.y + sprite.height > container.height) {
            sprite.y = container.height - sprite.height;
            colision = "bottom";
        }

        // Devuelve colision
        return colision;
    } //Fin contain

    /**
     * Función estática que comprueba si dos objetos están colisionando.
     * @param   {object}   r1 Objeto1
     * @param   {object}   r2 Objeto2
     * @returns {boolean} Devuelve true si hay colisión, false si no.
     */
    static checkColision(r1, r2) {

        // Variables necesarias para el cálculo
        var hit, anchoCombinado, altoCombinado, vx, vy;

        // Inicialización a falso, no hay colisión
        hit = false;

        // Cálculo de centros de sprites
        r1.centerX = r1.x + r1.width / 2;
        r1.centerY = r1.y + r1.height / 2;
        r2.centerX = r2.x + r2.width / 2;
        r2.centerY = r2.y + r2.height / 2;

        // Cálculo de medias dimensiones
        r1.halfWidth = r1.width / 2;
        r1.halfHeight = r1.height / 2;
        r2.halfWidth = r2.width / 2;
        r2.halfHeight = r2.height / 2;

        // Cálculo vector distancia entre centros de sprites
        vx = r1.centerX - r2.centerX;
        vy = r1.centerY - r2.centerY;

        // Cálculo total de las medias dimensiones
        anchoCombinado = r1.halfWidth + r2.halfWidth;
        altoCombinado = r1.halfHeight + r2.halfHeight;

        
        // Cálculo colusión eje x
        if (Math.abs(vx) < anchoCombinado) {
            // Cálculo colisión eje y si colisión eje x
            if (Math.abs(vy) < altoCombinado) {
                // Hay colisión.
                hit = true;
            } else {
                // No hay colisión
                hit = false;
            }
        } else {
            hit = false;
        }
        return hit;
    } // Fin checkColision

    /**
     * Función encargada de cambiar de nivel. Espera seis segundos, actualiza contadores, quita escenas y objetos y llama a la escena de cambio de nivel.
     */
    static cambiarDeNivel() {
        window.setTimeout(function () {
            cambiarDeNivel = false;

            enemigosMuertos = 0;
            state = stop;
            escenaJuego.visible = false;
            escenaJuego.removeChild(escenario);
            escenaJuego.removeChild(jugador);
            stage.removeChild(escenaJuego);
            inicializarEscenaNivel();
            enemigosAMatar += 3;
        }, 6000);


    }

    /**
     * Función ajax con jQuery para guardar la puntuación en la base de datos a través de POST
     * @param {string} nombreJugador El nombre del jugador
     * @param {string} puntos        La puntuación que ha conseguido
     * @param {string} fecha         La fecha en la que se ha conseguido la puntuación
     */
    static guardarPuntuacion(nombreJugador, puntos, fecha) {

        var jqxhr = $.post("../../pag/jugar-des/insertarRanking.php", {
            user: nombreJugador,
            fecha: fecha,
            puntuacion: puntos
            
        }).done(function () {
            alert("¡Partida finalizada!\rTu puntuación será guardada.");

        });
    } // Fin guardarPuntuación
} // Fin Utiles

