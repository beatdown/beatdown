/**
 * Esta función es la encarga de reconocer teclas pulsadas y realizar acciones en consecuencia.
 */
function asociarTeclas() {
    // Captura WASD
    var left = keyboard(65), //'A'
        up = keyboard(87), //'W'
        right = keyboard(68), //'D'
        down = keyboard(83), //'S'
        spaceBar = keyboard(32), //Barra espaciadora
        p = keyboard(80); // 'P'

    // Izquierda
    left.press = function () {
        jugador.vx = -VELOCIDAD * jugador.velocidad;
    };
    left.release = function () {
        if (!right.isDown) {
            jugador.vx = 0;
        } else if (right.isDown) {
            jugador.vx = VELOCIDAD * jugador.velocidad;
        }
    };

    // Derecha
    right.press = function () {
        jugador.vx = VELOCIDAD * jugador.velocidad;
    };
    right.release = function () {
        if (!left.isDown) {
            jugador.vx = 0;
        } else if (left.isDown) {
            jugador.vx = -VELOCIDAD * jugador.velocidad;
        }
    };

    // Arriba
    up.press = function () {
        jugador.vy = -VELOCIDAD * jugador.velocidad;
    };
    up.release = function () {
        if (!down.isDown) {
            jugador.vy = 0;
        } else if (up.isDown) {
            jugador.vy = VELOCIDAD * jugador.velocidad;
        }
    };

    // Abajo
    down.press = function () {
        jugador.vy = VELOCIDAD * jugador.velocidad;
    };
    down.release = function () {
        if (!up.isDown) {
            jugador.vy = 0;
        } else if (down.isDown) {
            jugador.vy = -VELOCIDAD * jugador.velocidad;
        }
    };


    // Barra espaciadora *cambio de arma*
    spaceBar.release = function () {
        if(jugador.armas[1]!=null){
           if (jugador.arma_seleccionada == 0) {
            jugador.arma_seleccionada = 1;

        } else {
            jugador.arma_seleccionada = 0;
        }

        TextosUI.actualizarArma();
        TextosUI.actualizarMunicion();
        jugador.cambiarSprite(jugador.armas[jugador.arma_seleccionada].spriteJugador);
           }
        
    };
    
    // P, cambio de arma
    p.release = function(){
        if (smoothie.paused==true) {
            smoothie.resume();
        }else{
            smoothie.pause();
        }
    };

}
