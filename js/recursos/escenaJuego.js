/**
 * Esta función es llamada cada vez que vayamos a inicializar una ronda. Aquí se escoge un mapa
 * y un hefe aleatorio (si procede), se hace la escena interactiva a eventos de ratón, 
 * se crean los enemigos pertinenetes y se coloca todo dentro de sus container antes de
 * cambiar el estado de juego a play.
 */
function inicializarEscenaJuego() {
    
    // Generación de containers y escenarios
    escenaJuego = new Container();
    generarMapa = parseInt(Math.floor(Math.random() * mapasDisponibles.length));
    escenario = new Sprite(mapasDisponibles[generarMapa]);    
    

    escenaJuego.addChild(escenario);
    escenaJuego.addChild(jugador);
   
    // Generación de jefe, si procede.
    if(nivel%jefeCadaRonda==0){
        var pistolaJefe = new PistolaJefe("", resources["img/disparo.png"].texture, "Pistola \"Lola\"", "Pistola", (nivel*3)/2, 10, 10, "∞");
        let valoresNivel=1+0.08*nivel;
        var generarJefe = parseInt(Math.floor(Math.random() * jefesDisponibles.length));
        jefe = new Jefe(jefesDisponibles[generarJefe], nivel*100, VELOCIDAD, valoresNivel, valoresNivel, valoresNivel, valoresNivel, valoresNivel, pistolaJefe);
        jefe.x=400;
        jefe.y=200;
        jefe.scale.set(0.5, 0.5);
        jefe.anchor.set(0.5, 0.5);
        escenaJuego.addChild(jefe);
        hayJefe=true;
    }
    
    // Escenario interactivo a eventos de ratón.
    escenaJuego.interactive = true;
    escenaJuego.on("mousedown", function (mouseData) {
        if ((jugador.armas[jugador.arma_seleccionada] == pistola) || (jugador.armas[jugador.arma_seleccionada].municion >= 1)) {
            jugador.disparar();
        }
    });
    
    // Genereación inicial de enemigos a derrotar por el jugador.
    for (let i = 0; i < enemigosAMatar/2; i++) {
        Enemigo.crearEnemigo();
    }
    
    // Añadimos la escena de juego, se hace visible, se inicializan sus textos y se comienza el juego.
    stage.addChild(escenaJuego);
    escenaJuego.visible = true;
    TextosUI.inicializarTextos();
    state = play;
    generarEnemigos=true;
}
