//Usado en el método 2 de control de fotogramas, a través de funciones nativas de JS
/*var fps = 30;
var now;
var then = Date.now();
var interval = 1000 / fps;
var delta;*/

function gameLoop() {
    
    //Usado en método2
    //Loop this function 60 times per second
    /*requestAnimationFrame(gameLoop);

    now = Date.now();
    delta = now - then;

    if (delta > interval) {
        then = now - (delta % interval);
        //Update the current game state
        if (escenainicio.visible != true)
            state();


        //Render the stage
        renderer.render(stage);
    }
    */
    //Fin método 2
    
    
    //Usado en el Metodo 1, smoothie.js
    //Update the current game state
     if(escenainicio.visible != true ){
        state();
     }
    //Render the stage
    renderer.render(stage);
    
    //Fin método 1

} // Fin gameLoop

/**
 * Es la función donde se agrupa toda al lógica del juego, para una mejor comprensión de la misma,
 * pese a la documentación en código, rogamos acudir al apartado "Desarrollo y codificación"
 * de la memoria del proyecto.
 * 
 * Esta función es llamada tantas veces por segundo como se especificque en el objeto Smoothie.
 */
function play() {
    
    // Comprobamos la colisión del jugador con los items de la escena.
    Utiles.checkItemsSuelo();
    
    // Comprobamos las colisiones de los disparos y actualizamos vidas e interfaz si procede.
    Utiles.checkDisparos();
    
    // Se rota al jugador.
    Utiles.rotarJugador();
    
    // Se comprueban colisiones jugador-enemigo, actualizan vidas, enemigos e interfaz (si procede) y se supera la ronda o pierde el juego (cuando proceda)
    Utiles.checkEnemigos();

    // Comprobamos que nuestro jugador no sale de los límites del mapa.
    Utiles.contain(jugador, limitesMapas[generarMapa]);

    // Generamos enemigos hasta que proceda    
    while (Object.keys(enemigos).length < parseInt(enemigosAMatar/3) && enemigosMuertos+parseInt(enemigosAMatar/3) <= enemigosAMatar) {
        Enemigo.crearEnemigo();
    }
    
    // Usado en el medidor de FPS.
    meter.tick();
} // fin play

/**
 * Función encargada de la transición a la escena de fin de juego, 
 * llamada cuando nos quedamos sin salud.
 */
function end() {
    escenaJuego.visible = false;
    fin.visible = true;
    createjs.Sound.stop(soundID4);
}
