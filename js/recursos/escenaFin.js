// Declaración de variables
var mensaje, mensajepuntos, style;

/**
 * Inicialización de la escena que nos indica el fin de la partida.
 */
function inicializarEscenaFin() {
    fin = new PIXI.Container();
    fin.visible = false;
    stage.addChild(fin);

    style = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 36,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#000000',
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6
    });

    mensaje = new PIXI.Text("¡HAS MUERTO!", style);
    mensaje.x = 375;
    mensaje.y = 200;

    fin.addChild(mensaje);
} // Fin inicializarEscenaFin

/**
 * Función encargada de mostrar la puntuación final, y enviar la información necesaria a Utiles.guardarPuntuacion
 * para que la partida quede registrada en la base de datos.
 */
function actualizarPuntuacion() {

    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var fecha = d.getFullYear() + '-' +
        (('' + month).length < 2 ? '0' : '') + month + '-' +
        (('' + day).length < 2 ? '0' : '') + day;

    mensajepuntos = new PIXI.Text("Has conseguido " + puntuacion + " puntos", style);
    mensajepuntos.x = 250;
    mensajepuntos.y = 325;

    fin.addChild(mensajepuntos);
    
    Utiles.guardarPuntuacion(jugador.nombre, puntuacion, fecha);
} // Fin actualizarPuntuacion
