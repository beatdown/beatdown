/**
 * La función se encarga de revoger todas als cookies y mostrarlas en un alert
 * ocupando cada línea una cookie y su valor.
 */
function listCookies(){
  var theCookies = document.cookie.split(';');
  var cookieString = '';

  for (var i = 1 ; i <= theCookies.length; i++) {
    cookieString += i + ' ' + theCookies[i-1] + "\n";
  }

  alert(cookieString);
}

/**
 * Devuelve en un array las cookies
 * @return {String[} - El array con los elementos
 */
function cookiesToArray(){
  var cadenaCookies = document.cookie.split(';');
  var arrayCookies = [];
  for (var i = 0; i < cadenaCookies.length; i++) {
    arrayCookies.push(cadenaCookies[i].split("="));
  }
  return arrayCookies;
}

/**
 * Recibe nombre, valor y días de expiración y crea una cookie con ellos.
 * @param {String} cname - El nombre de la cookie
 * @param {String} cvalue - El valor de la cookie
 * @param {Number} exdays - Días en los que expira la cookie
 */
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/**
 * Recibe un nombre, lo busca en cookies. Si existe, devuelve el valor, si no devuelve null.
 * @param {String} name - Nombre de la cookie a buscar
 * @return {String} - El valor de la cookie
 * @return {null} - Devuelve null si no la encuentra
 */
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

/**
 * Recibe el nombre de una cookie y devuelve el índice de la cookie. Si no, devuelve -1
 * @param {String} cookieName - El nombre de la cookie
 * @return {Number} cookieStartsAt - El índice donde empieza la cookie a buscar
 */
function checkCookie(cookieName){
  var cookieStartsAt = document.cookie.indexOf(cookieName);
  return cookieStartsAt;
}

/**
 * Recibe el indice del inicio de la cookie y devuelve el valor de la misma
 * @param {Number} cookieStartsAt - El índice con el inicio del valor de la cookie
 * @return {String} - El valor de la cookie
 */
function leerCookie(cookieStartsAt){
  console.log(subcadenaCookies+"PAAAAAAAM");
  var inicio=subcadenaCookies.indexOf('=');
  var final=subcadenaCookies.indexOf(';');
  if (final==-1){
    subcadenaCookies = subcadenaCookies.substring(inicio+1);
    return subcadenaCookies;
  }else{
    subcadenaCookies = subcadenaCookies.substring(inicio+1, final);
    return subcadenaCookies;
  }
}

/**
 * Recibe el nombre de una cookie y la borra
 */
function eraseCookie(name) {
    createCookie(name,"",-1);
}
