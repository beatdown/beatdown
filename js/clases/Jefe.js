/**
* Objetos de tipo Jefe, los malos del juego.
* @extends Enemigo
*/
class Jefe extends Enemigo {

    /**
     * Genera un objeto de tipo Jefe
     * @param {texture} sprite           La textura del boss
     * @param {number} vida             La vida del boss
     * @param {number} velocidad        La velocidad del boss
     * @param {number} resistencia      Multiplicador resistencia
     * @param {number} penetracion      Multiplicador daño
     * @param {number} velocidadAtaque  Multiplicador velocidad de ataque
     * @param {number} velocidadRecarga Multiplicador velocidad de recarga
     * @param {number} dañoEnemigo      daño
     * @param {pistolaJefe} arma             El arma que porta el boss
     */
    constructor(sprite, vida, velocidad, resistencia, penetracion, velocidadAtaque, velocidadRecarga, dañoEnemigo, arma) {
        super(sprite, vida, velocidad, resistencia, penetracion, velocidadAtaque, velocidadRecarga, dañoEnemigo);
        this.arma=arma;
    }

    /**
     * Escoge un arma de un array y la dropea para que se pueda coger.
     */
    drop() {
        var valorRandom = parseInt(Math.floor(Math.random() * armasDisponibles.length));
        var item = new ItemSuelo(armasDisponibles[valorRandom].imagen, "arma", valorRandom);
        item.x = this.x;
        item.y = this.y;
        item.scale.set(3, 3);
        item.anchor.set(0.5, 0.5);
        itemsSuelo.push(item);
        escenaJuego.addChild(item);
    }
    
    /**
     * Dispara a la posición del jugador.
     */
    disparar() {
        this.arma.disparar(this.rotation, {
            x: this.position.x + Math.cos(this.rotation) * 20,
            y: this.position.y + Math.sin(this.rotation) * 20});
    }

}
