/**
* La clase disparo representa la bala que se desplaza por el mapa e impacta enemigos.
* @extends Arma
*/
class Disparo extends PIXI.Sprite{
    /**
     * Crea un objeto de tipo Disparo cogiendo las propiedades del arma de la que hereda.
     * @param {object} arma El objeto de tipo arma a aprtir del cual se crea un disparo.
     */
    constructor(arma){
        super(arma.spriteDisparo);
        
        this.velocidad=arma.velDisparo;
        this.daño=arma.daño;
        this.scale.set(0.02,0.02);
        //console.log("ENEMIGOS   :    " + Object.keys(enemigos).length);


        

    }
    
}