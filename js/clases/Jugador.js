/**
* El jugador, el personaje que manejamos
* @extends Personaje
*/
class Jugador extends Personaje {
    /**
     * Genera un objeto de tipo Jugador
     * @param {texture} sprite          El sprite del jugador
     * @param {number} vida            La vida del jugador
     * @param {number} velocidad       Multiplicador velocidad
     * @param {number} resistencia     Multiplicador resistencia
     * @param {number} penetracion     Multiplicador daño
     * @param {number} velocidadAtaque Multiplicador velocidad ataque
     * @param {string} nombre          Nombre, cogido de cookie
     * @param {number} vidaMax         Vida máxima que puede tener
     */
    constructor(sprite, vida, vidaMax, velocidad, resistencia, penetracion, velocidadAtaque, nombre) {
        super(sprite, vida, velocidad, resistencia, penetracion, velocidadAtaque);

        this.nombre = nombre;
        this.vidaMax = vidaMax;
        this.armas = [];
        this.arma_seleccionada = 0;

    }
    
    /**
     * Ejecuta un disparo a la posición del ratón
     */
    disparar() {
        this.armas[this.arma_seleccionada].disparar(this.rotation, {
            x: this.position.x + Math.cos(this.rotation) * 20,
            y: this.position.y + Math.sin(this.rotation) * 20});
    }

}
