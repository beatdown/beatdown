/**
 * La calse enemigo a aprtir de la cual se generan todos los enemigos del juego
 * @extends Personaje
 */


class Enemigo extends Personaje {
    /**
     * Crea un nuevo objeto de tipo enemigo
     * @param {PIXI.Sprite} sprite           El sprite del enemigo
     * @param {number} vida             La cantidad de vida de la que dispone el enemigo
     * @param {number} velocidad        Multiplicador de la velocidad del enemigo
     * @param {number} resistencia      Multiplicador de la resitencia, afecta a la cantidad de daño recibida
     * @param {number} penetracion      Multiplicador de penetración, afecta a la cantidad de daño realizada
     * @param {number} velocidadAtaque  Multiplicador de la velocidad de ataque, afecta a la velocidad con la que ataca el enemigo
     * @param {number} velocidadRecarga Cantidad de tiempo que tarda el enemigo en recargar su arma (si la tiene)
     * @param {number} dañoEnemigo      Cantidad de daño que realiza el enemigo a nuestro jugador
     */
    constructor(sprite, vida, velocidad, resistencia, penetracion, velocidadAtaque, velocidadRecarga, dañoEnemigo) {
        super(sprite, vida, velocidad, resistencia, penetracion, velocidadAtaque, velocidadRecarga);
        this.dañoEnemigo = dañoEnemigo;
    }

    /**
     * Función que desplaza el objeto de tipo enemigo al personaje, modificando ligeramente su elocidads
     */
    moverEnemigo() {
        this.x += this.vx = Math.sign(jugador.x - this.x) * (Math.floor(Math.random() * (VELOCIDAD * this.velocidad)) + 1);
        this.y += this.vy = Math.sign(jugador.y - this.y) * (Math.floor(Math.random() * (VELOCIDAD * this.velocidad)) + 1);

    }
    
    /**
     * Tira los dados, si cae 0 o 1 suelta munición o salud, según corresponda, en la posición del enemigo.
     */
    drop() {
        var valorRandom=parseInt(Math.floor(Math.random() * 6));
        //console.log("RAND: "+valorRandom);
        switch (valorRandom) {
            case 0:
                var atributo = parseInt(Math.floor((Math.random() * 10) + 10)*(nivel/2));
                var item = new ItemSuelo(resources["img/ammo.png"].texture, "municion", atributo);
                item.x = this.x;
                item.y = this.y;
                item.scale.set(0.2, 0.2);
                item.anchor.set(0.5, 0.5);
                itemsSuelo.push(item);
                escenaJuego.addChild(item);
                //console.log("¡HA CAIDO MUNICIÓN!: " + item.atributo);
                break;

            case 1:
            case 2:
                var atributo = parseInt(Math.floor((Math.random() * 10) + 2)*(nivel/2));
                var item = new ItemSuelo(resources["img/health.png"].texture, "salud", atributo);
                item.x = this.x;
                item.y = this.y;
                item.scale.set(0.2, 0.2);
                item.anchor.set(0.5, 0.5);
                itemsSuelo.push(item);
                escenaJuego.addChild(item);
                //console.log("¡HA CAIDO SALUD!: " + item.atributo);
                break;

            default:
                break;
        }



    }

    static crearEnemigo() {
        let espacio = 48,
            xOffset = 150;
        let valoresNivel=1+0.07*nivel;
        en = new Enemigo(resources["img/enemigo.png"].texture, 100+nivel*10, valoresNivel, valoresNivel, valoresNivel, valoresNivel, valoresNivel, 0.1+0.08*nivel);
        
        let x = espacio * Math.floor((Math.random() * 10) + 1) + xOffset;
        let y = Math.floor(Math.random() * 400) + 100;

        en.x = x;
        en.y = y;

        en.scale.set(0.25, 0.25);
        en.anchor.set(0.5, 0.5);
        enemigos.push(en);
        escenaJuego.addChild(en);
    }
}
