/**
* Son los objetos que sueltan los enemigos
* @extends PIXI.Sprite
*/
class ItemSuelo extends PIXI.Sprite{
    
    /**
     * Genera un objeto de tipo ItemSuelo
     * @param {texture} sprite   La imagen del item en el suelo
     * @param {string} tipo     El tipo de item
     * @param {number} atributo Sus atributos, sea la cantidad de munición que aporat, por ejemplo
     */
    constructor(sprite, tipo, atributo){
        super(sprite);
        this.tipo=tipo;
        this.atributo=atributo;
        
    }
    
}