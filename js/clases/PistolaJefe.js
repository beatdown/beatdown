/**
* El arma que usa el jefe
* @extends Arma
*/
class PistolaJefe extends Arma{
    /**
     * Genera un objeto de tipo pistola
     * @param {texture} spriteJugador La imagen que muestra el jugador al portarla
     * @param {texture} spriteDisparo El sprite del disparo
     * @param {string} nombre        El nombre del arma
     * @param {string} tipo          El tipo de arma
     * @param {number} daño          Daño del arma
     * @param {number} velDisparo    Velocidad de disparo del arma
     * @param {number} cadencia      Cadencia del arma
     * @param {number} municion      Munición disponible
     */
    constructor(spriteJugador, spriteDisparo, nombre, tipo, daño, velDisparo, cadencia, municion){
        super(spriteJugador, spriteDisparo, nombre, tipo, daño, velDisparo, cadencia, municion);
    }
    
    
    /**
     * Esta función crea un objeto de tipo disparo, lo situa en una posición inicial, lo rota, lo añade al escenario y lo agrega a un array de objetos Disparo para llevar el control de los mismos
     * @param {PIXI.Sprite.rotation} rotation      La rotación actual del jugador
     * @param {PIXI.Point}   startPosition La posición inicial (x,y) del disparo
     */
    disparar(rotation, startPosition) {
        var disparo = new Disparo(this);
        disparo.position.x = startPosition.x;
        disparo.position.y = startPosition.y;
        disparo.rotation = rotation;
        escenaJuego.addChild(disparo);
        var instance = createjs.Sound.play(soundID);
        instance.volume = 0.25;
        disparosEnemigos.push(disparo);

    }
    
    
    
}