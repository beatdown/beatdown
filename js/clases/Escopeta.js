/**
* Esta clase genera objetos Escopeta
* @extends Arma
*/
class Escopeta extends Arma {
    constructor(spriteJugador, spriteDisparo, nombre, tipo, daño, velDisparo, cadencia, municion, imagen){
        super(spriteJugador, spriteDisparo, nombre, tipo, daño, velDisparo, cadencia, municion, imagen);
    }
    /**
     * Esta función crea un objeto de tipo disparo, lo situa en una posición inicial, lo rota, lo añade al escenario y lo agrega a un array de objetos Disparo para llevar el control de los mismos
     * @param {PIXI.Sprite.rotation} rotation      La rotación actual del jugador
     * @param {PIXI.Point}   startPosition La posición inicial (x,y) del disparo
     */
    disparar(rotation, startPosition) {
        var grados =  Math.PI *  0.0610865; //3.5 grados en radianes
        rotation -=grados;
        
        for (let i = 1; i<=3; i++) {
            
            var disparo = new Disparo(this);
            disparo.position.x = startPosition.x;
            disparo.position.y = startPosition.y;
            disparo.rotation = rotation;
            escenaJuego.addChild(disparo);
            disparos.push(disparo);
            this.municion -= 1;
            rotation+=grados;
        }
        
        var instance =createjs.Sound.play(soundID);
        instance.volume = 0.25;
       
        TextosUI.actualizarMunicion();

    }

}
