/**
 * La clase Arma es el objeto que puede equipar al personaje con el que dispara
 */
class Arma {
    /**
     * Crea un objeto de tipo Arma
     * @param {PIXI.Sprite} spriteJugador Textura que se cargará en el jugador cuando cambie de arma
     * @param {PIXI.Sprite} spriteDisparo El sprite de la bala
     * @param {String}      nombre        El nombre del arma
     * @param {string}      tipo          El tipo de arma
     * @param {number}      daño          El daño que hace el arma
     * @param {number}      velDisparo    La velocidad a la que se mueve el disparo
     * @param {number}      cadencia      La velocidad a la que dispara el arma
     * @param {number}      municion      Cantidad de disparos disponibles
     * @param {Sprite}    imagen        Imagen item suelo
     */
    constructor(spriteJugador, spriteDisparo, nombre, tipo, daño, velDisparo, cadencia, municion, imagen) {

        this.spriteJugador = spriteJugador;
        this.spriteDisparo = spriteDisparo;
        this.nombre = nombre;
        this.tipo = tipo;
        this.daño = daño;
        this.velDisparo = velDisparo;

        this.cadencia = cadencia;
        this.municion = municion;
        this.imagen=imagen;

    }


}
