/**
* Crea un objeto de tipo personaje del que heredan todos los personajes del juego, tanto jugador como enemigos.
* @extends PIXI.Sprite
*/

class Personaje extends PIXI.Sprite{
    /**
     * Crea un objeto de tipo Personaje
     * @param {PIXI.Sprite} sprite          El sprite del personaje
     * @param {number} vida            La cantidad de vida de la que dispone el personaje
     * @param {number} velocidad       Multiplicador de la velocidad global, individualizando así la velocidad de cada Personaje
     * @param {number} resistencia     Multiplicador de la resistencia del personaje, disminuyendo la cantidad de vida que se le resta
     * @param {number} penetracion     Multiplicador que aumenta el daño realizado
     * @param {number} velocidadAtaque Multiplicador que aumenta la velocidad a la que ataca el personaje
     */
    constructor(sprite, vida, velocidad, resistencia, penetracion, velocidadAtaque){
        super(sprite);
        
        this.vida=vida;
        this.velocidad=velocidad;
        this.resistencia=resistencia;
        this.penetracion=penetracion;
        this.velocidadAtaque=velocidadAtaque;
    }
    
    /**
     * Con esta función se rota al personaje hacia el punto indicado
     * @param {PIXI.Point} punto Las coordenadas XY a las que queremos apuntar
     */
    rotar(punto){
        this.rotation = Math.atan2(punto.y - this.y, punto.x - this.x);
    }
    
    /**
     * Con esta función el personaje recibe el daño indicado
     * @param {PIXI.Point} punto Las coordenadas XY a las que queremos apuntar
     */
    recibirDaño(daño){
        this.vida-=daño;
    }
    
    /**
     * Campia el sprite del personaje por el recibido
     * @param {texture} sprite El sprite a mostrar
     */
    
    cambiarSprite(sprite){
        this.texture=sprite;
    }
}